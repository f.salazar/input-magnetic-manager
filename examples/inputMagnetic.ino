/*NOTE
The Input Magneticservice must be started after having performed a closing event or, failing that, start the service when the user wants to use it.
*/

#include <Arduino.h>
#include <InputMagnetic.h>

InputMagnetic imMagnetic;
byte input = 1;
byte pin = 32;

void inputHandlerMagnetic(byte input, byte event) // OPEN = 0 //CLOSE = 1 //RELAUNCH = 2
{
  switch (event)
  {
  case OPEN
    Serial.println("In this event you can execute an action when the input is open");
    break;
  case CLOSE
    Serial.println("In this event you can execute an action when the input is closed after being opened");
    break;
  case RELAUNCH
    Serial.println("In this event you can execute an action when the input is closed and then opened and the closing condition has not been met.");
    break;
  
  default:
    break;
  }
}

void setup()
{
  Serial.begin(115200);
  imMagnetic.setupMagneticInput(input, pin, INPUT_PULLDOWN);
  imMagnetic.setOpenCallback(input, inputHandlerMagnetic);
  imMagnetic.setCloseCallback(input, inputHandlerMagnetic);
  imMagnetic.setRelaunchCallback(input, inputHandlerMagnetic);
  imMagnetic.startService(input);
}

void loop()
{
  imMagnetic.loopInputMagnetic();
}