#ifndef INPUT_MAGNETIC_H
#define INPUT_MAGNETIC_H

#include "Arduino.h"

enum InputMagneticEvents
{
  OPEN,
  CLOSE,
  RELAUNCH
};

class InputMagnetic
{
  typedef struct InputMagneticConfig
  {
    bool enabled = true;
    bool status = false;
    bool closed = false;
    unsigned long lastClose;
    uint16_t targetCloseTime = 2000;
    bool open = false;
    bool relaunch = false;
    bool callbackOpenStatus = false;
    bool callbackCloseStatus = false;
    bool callbackRelaunchStatus = false;
    void (*callbackOpen)(byte input, byte event);
    void (*callbackClose)(byte input, byte event);
    void (*callbackRelaunch)(byte input, byte event);
    byte pin;
    byte input;
    int mode;
  } InputMagneticConfig_t;

private:
  static void _inputActionHandler(byte input, byte event);
  static void _inputService(byte input);
  static byte _readStatus(byte input);
  static int _getInputMode(byte input);
  static int _getModeEvaluation(byte input);
  static InputMagneticConfig_t _inputMagneticConfig[10];
  static int _loopPreviousTime;
  static int MAGNETIC_LOOP_FILTER_TIME;
  static int MAGNETIC_INPUT_COUNT;

public:
  InputMagnetic(uint8_t inputs = 1);
  void setupMagneticInput(byte input, byte pin, int mode = INPUT_PULLDOWN);
  void setOpenCallback(byte input, void (*callbackOpen)(byte input, byte event));
  void setCloseCallback(byte input, void (*callbackClose)(byte input, byte event));
  void setRelaunchCallback(byte input, void (*callbackRelaunch)(byte input, byte event));
  void startService(byte input);
  void enableMagneticInput(byte input);
  void disableMagneticInput(byte input);
  void stopService(byte input);
  void setCloseTime(byte input, uint16_t targetTime);
  void setGlobalFilterTime(uint16_t targetTime);
  byte readStatus(byte input);
  void loopInputMagnetic();
};

#endif