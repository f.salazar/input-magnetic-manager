#include <InputMagnetic.h>

InputMagnetic::InputMagneticConfig_t InputMagnetic::_inputMagneticConfig[10];
int InputMagnetic::MAGNETIC_INPUT_COUNT;
int InputMagnetic::MAGNETIC_LOOP_FILTER_TIME = 80;
int InputMagnetic::_loopPreviousTime = millis();

InputMagnetic::InputMagnetic(uint8_t inputs)
{
  MAGNETIC_INPUT_COUNT = inputs;
}

int InputMagnetic::_getInputMode(byte input)
{
  return _inputMagneticConfig[input].mode;
}

int InputMagnetic::_getModeEvaluation(byte input)
{
  int evaluation = 0;
  int inputMode = _getInputMode(input);
  if (inputMode == INPUT || inputMode == INPUT_PULLDOWN)
    evaluation = 1;
  return evaluation;
}

byte InputMagnetic::_readStatus(byte input)
{
  byte pin = _inputMagneticConfig[input].pin;
  return digitalRead(pin);
}

void InputMagnetic::_inputActionHandler(byte input, byte event)
{
  switch (event)
  {
  case OPEN:
    if (_inputMagneticConfig[input].callbackOpenStatus)
      _inputMagneticConfig[input].callbackOpen(input, event);
    break;
  case CLOSE:
    if (_inputMagneticConfig[input].callbackCloseStatus)
      _inputMagneticConfig[input].callbackClose(input, event);
    break;
  case RELAUNCH:
    if (_inputMagneticConfig[input].callbackRelaunchStatus)
      _inputMagneticConfig[input].callbackRelaunch(input, event);
    break;

  default:
    Serial.println("WARNING UNHANDLED EVENT " + String(event) + " FROM INPUT " + String(input));
    break;
  }
}

void InputMagnetic::_inputService(byte input)
{
  if (!_inputMagneticConfig[input].enabled)
    return;
  if (_inputMagneticConfig[input].status)
  {
    byte status = _readStatus(input);           // 1 / 0
    int evaluation = _getModeEvaluation(input); // 1 | 0
    bool active = status == evaluation;         // false si se abre // true si esta cerrado
    unsigned long currentTime = millis();
    uint16_t targetCloseTime = _inputMagneticConfig[input].targetCloseTime;

    if (!active && !_inputMagneticConfig[input].open)
    {
      _inputActionHandler(input, OPEN);
      _inputMagneticConfig[input].open = true;
    }
    else if (active && _inputMagneticConfig[input].open && !_inputMagneticConfig[input].closed)
    {
      _inputMagneticConfig[input].closed = true;
      _inputMagneticConfig[input].relaunch = false;
      _inputMagneticConfig[input].lastClose = millis();
    }
    else if (active && _inputMagneticConfig[input].closed && (currentTime - _inputMagneticConfig[input].lastClose) >= targetCloseTime)
    {
      _inputActionHandler(input, CLOSE);
      _inputMagneticConfig[input].closed = false;
      _inputMagneticConfig[input].open = false;
      _inputMagneticConfig[input].status = false;
    }
    else if (!active && _inputMagneticConfig[input].open && _inputMagneticConfig[input].closed && !_inputMagneticConfig[input].relaunch)
    {
      _inputActionHandler(input, RELAUNCH);
      _inputMagneticConfig[input].lastClose = millis();
      _inputMagneticConfig[input].closed = false;
      _inputMagneticConfig[input].relaunch = true;
    }
  }
}

void InputMagnetic::setupMagneticInput(byte input, byte pin, int mode)
{
  _inputMagneticConfig[input].pin = pin;
  _inputMagneticConfig[input].input = input;
  _inputMagneticConfig[input].mode = mode;
  _inputMagneticConfig[input].enabled = true;
  pinMode(pin, mode);
  Serial.println("[INPUT][MAGNETIC][SETUP]: input " + String(input) + " on pin " + String(pin));
}

void InputMagnetic::setOpenCallback(byte input, void (*callbackOpen)(byte input, byte event))
{
  _inputMagneticConfig[input].callbackOpenStatus = true;
  _inputMagneticConfig[input].callbackOpen = callbackOpen;
}

void InputMagnetic::setCloseCallback(byte input, void (*callbackClose)(byte input, byte event))
{
  _inputMagneticConfig[input].callbackCloseStatus = true;
  _inputMagneticConfig[input].callbackClose = callbackClose;
}

void InputMagnetic::setRelaunchCallback(byte input, void (*callbackRelaunch)(byte input, byte event))
{
  _inputMagneticConfig[input].callbackRelaunchStatus = true;
  _inputMagneticConfig[input].callbackRelaunch = callbackRelaunch;
}

void InputMagnetic::startService(byte input)
{
  _inputMagneticConfig[input].status = true;
}

void InputMagnetic::enableMagneticInput(byte input)
{
  _inputMagneticConfig[input].enabled = true;
}

void InputMagnetic::disableMagneticInput(byte input)
{
  _inputMagneticConfig[input].enabled = false;
}

void InputMagnetic::stopService(byte input)
{
  _inputMagneticConfig[input].status = false;
  _inputMagneticConfig[input].open = false;
  _inputMagneticConfig[input].closed = false;
  _inputMagneticConfig[input].relaunch = false;
}

void InputMagnetic::setCloseTime(byte input, uint16_t targetTime)
{
  _inputMagneticConfig[input].targetCloseTime = targetTime;
}

void InputMagnetic::setGlobalFilterTime(uint16_t targetTime)
{
  MAGNETIC_LOOP_FILTER_TIME = targetTime;
}

byte InputMagnetic::readStatus(byte input)
{
  return _readStatus(input);
}

void InputMagnetic::loopInputMagnetic()
{
  if (millis() - _loopPreviousTime >= MAGNETIC_LOOP_FILTER_TIME)
  {
    for (size_t i = 1; i <= MAGNETIC_INPUT_COUNT; i++)
    {
      _inputService(i);
    }
    _loopPreviousTime = millis();
  }
}